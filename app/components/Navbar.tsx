'use client';
import { Box, Container, Group, Paper, Text } from '@mantine/core';
import { IconHeadset, IconHome, IconMapPin } from '@tabler/icons-react';
import Image from 'next/image';
import '@mantine/core/styles.css';

function Navbar() {
  return (
    <Container
      fluid
      mt={20}
      style={{
        zIndex: 100,
        display: 'flex',
      }}
    >
      <Paper shadow="xl" radius={50} w={'90vw'} px={50} py={10} mx={'auto'}>
        <Box
          style={{
            display: 'flex',
            width: '100%',
          }}
        >
          <Image src={'/Logo.png'} style={{ marginRight: 40 }} width={200} height={40} alt="Logo" />
          <Group justify="space-between" w="100%">
            <Group>
              <IconHome color="#293C76" size={30} />
              <IconHeadset color="#293C76" size={30} />
              <IconMapPin color="#293C76" size={30} />
              <Text c="#293C76" fw={600} size="lg">
                Ваш город
              </Text>
            </Group>

            <Group gap={50}>
              <Text c={'#293C76'} fw={600} size="lg">
                О проекте
              </Text>
              <Text c={'#293C76'} fw={600} size="lg">
                Выбрать специалиста
              </Text>
              <Text c={'#293C76'} fw={600} size="lg">
                Стать специалистом
              </Text>
              <Text c={'#293C76'} fw={600} size="lg">
                Войти
              </Text>
            </Group>
          </Group>
        </Box>
      </Paper>
    </Container>
  );
}

export default Navbar;
