'use client';
import { useEffect, useRef, useState } from 'react';
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import Image from 'next/image';
import {
  IconAntennaBars1,
  IconAntennaBars2,
  IconAntennaBars3,
  IconAntennaBars4,
  IconAntennaBars5,
  IconCamera,
  IconCameraOff,
  IconCameraRotate,
  IconFileArrowRight,
  IconMicrophone,
  IconScreenShare,
} from '@tabler/icons-react';
import { ActionIcon, Box, Button, Stack, Text, TextInput } from '@mantine/core';
import Link from 'next/link';
import WebRTCIssueDetector from 'webrtc-issue-detector';

const firebaseConfig = {
  apiKey: 'AIzaSyDXFhWl_NYVCu8C0i-EpCfU8fDCKfljyUw',
  authDomain: 'test-webrtc-56325.firebaseapp.com',
  projectId: 'test-webrtc-56325',
  storageBucket: 'test-webrtc-56325.appspot.com',
  messagingSenderId: '745315528767',
  appId: '1:745315528767:web:2fba6bb01cfc54f3b8488a',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function RoomPage() {
  const localCamRef = useRef<HTMLVideoElement>(null);
  const remoteCamRef = useRef<HTMLVideoElement>(null);
  const localStreamRef = useRef<MediaStream | null>(null);
  const remoteStreamRef = useRef<MediaStream | null>(null);
  const [isCamOn, setIsCamOn] = useState<boolean>(true);
  const [connectionScore, setConnectionScore] = useState<number>(0);
  const servers = {
    iceServers: [
      {
        urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
      },
    ],
    iceCandidatePoolSize: 10,
  };
  const [pc, setPc] = useState<RTCPeerConnection | null>(null);
  const [callId, setCallId] = useState<string>('');

  const webRtcIssueDetector = new WebRTCIssueDetector({
    onIssues: (issues) => {
      console.log(issues);
    },
    onNetworkScoresUpdated(score) {
      console.log(score);
      setConnectionScore(score.outbound as number);
    },
  });
  webRtcIssueDetector.watchNewPeerConnections();

  const toggleCam = async () => {
    if (isCamOn) {
      localCamRef.current!.srcObject = null;
      setIsCamOn(false);
    } else {
      localCamRef.current!.srcObject = localStreamRef.current;
      setIsCamOn(true);
    }
  };

  const Answer = async () => {
    if (pc === null) return;
    const callDoc = firebase.firestore().collection('calls').doc(callId);
    const answerCandidates = callDoc.collection('answerCandidates');
    const offerCandidates = callDoc.collection('offerCandidates');
    pc.onicecandidate = (event) => {
      event.candidate && answerCandidates.add(event.candidate.toJSON());
    };
    const callData = (await callDoc.get()).data();
    const offerDescription = callData?.offer;
    await pc.setRemoteDescription(new RTCSessionDescription(offerDescription));
    const answerDescription = await pc.createAnswer();
    await pc.setLocalDescription(answerDescription);
    const answer = {
      type: answerDescription.type,
      sdp: answerDescription.sdp,
    };
    await callDoc.update({ answer });
    offerCandidates.onSnapshot((snapshot: any) => {
      snapshot.docChanges().forEach((change: any) => {
        console.log(change);
        if (change.type === 'added') {
          let data = change.doc.data();
          pc.addIceCandidate(new RTCIceCandidate(data));
        }
      });
    });
  };
  const shareScreen = async () => {
    navigator.mediaDevices.getDisplayMedia().then((stream) => {
      const screenTrack = stream.getTracks()[0];
      const screenStream = new MediaStream([screenTrack]);
      localStreamRef.current = screenStream;
      localCamRef.current!.srcObject = screenStream;
      screenTrack.onended = function () {
        localCamRef.current!.srcObject = null;
      };
    });
  };
  const Call = async () => {
    if (!pc) return;
    const callDoc = firebase.firestore().collection('calls').doc();
    const offerCandidates = callDoc.collection('offerCandidates');
    const answerCandidates = callDoc.collection('answerCandidates');
    setCallId(callDoc.id);
    // Create offer
    pc.onicecandidate = (event) => {
      event.candidate && offerCandidates.add(event.candidate.toJSON());
    };
    const offerDescription = await pc.createOffer();
    await pc.setLocalDescription(offerDescription);

    const offer: any = {
      sdp: offerDescription.sdp,
      type: offerDescription.type,
    };

    await callDoc.set({ offer });

    callDoc.onSnapshot((snapshot) => {
      const data = snapshot.data();
      if (!pc.currentRemoteDescription && data?.answer) {
        const answerDescription = new RTCSessionDescription(data.answer);
        pc.setRemoteDescription(answerDescription);
      }
    });
    answerCandidates.onSnapshot((snapshot: any) => {
      snapshot.docChanges().forEach((change: any) => {
        if (change.type === 'added') {
          const candidate = new RTCIceCandidate(change.doc.data());
          pc.addIceCandidate(candidate);
        }
      });
    });
  };

  const StreamsSettings = async () => {
    if (!pc) return;
    try {
      await navigator.mediaDevices
        .getUserMedia({
          video: true,
          audio: true,
        })
        .then((stream) => {
          localStreamRef.current = stream;
        });
      remoteStreamRef.current = new MediaStream();
    } catch (error) {
      console.log(error);
    } finally {
      localStreamRef.current!.getTracks().forEach((track: any) => {
        pc.addTrack(track, localStreamRef.current!);
      });

      pc.ontrack = (event) => {
        event.streams[0].getTracks().forEach((track: any) => {
          remoteStreamRef.current!.addTrack(track);
        });
      };
      localCamRef.current!.srcObject = localStreamRef.current;
      remoteCamRef.current!.srcObject = remoteStreamRef.current;
    }
  };

  useEffect(() => {
    if (typeof window === 'undefined') return;
    setPc(new RTCPeerConnection(servers));
  }, []);
  useEffect(() => {
    if (!pc) return;
    StreamsSettings();
  }, [pc]);
  return (
    <Box
      pt={50}
      style={{
        width: '100%',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        padding: 0,
        backgroundColor: '#021E39',
        alignItems: 'center',
        pt: 8,
      }}
    >
      <Box
        style={{
          display: 'flex',
          width: '90%',
          height: '85%',
          backgroundColor: '#133352',
        }}
      >
        <Box
          style={{
            width: '100%',
            height: '100%',
            display: 'flex',
            position: 'relative',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image src={'/caller.png'} width={300} height={300} alt="caller" />
          <video
            ref={localCamRef}
            id="webcamVideo"
            width="100%"
            height={'100%'}
            style={{
              transform: 'rotateY(180deg)',
              position: 'absolute',
            }}
            autoPlay
          ></video>
        </Box>
        <Box
          style={{
            padding: 5,
            width: 340,
            height: 340,
            position: 'absolute',
          }}
        >
          <Image src={'/caller.png'} width={200} height={200} alt="caller" />

          <video
            ref={remoteCamRef}
            id="remoteVideo"
            height={'100%'}
            style={{
              transform: 'rotateY(180deg)',
              position: 'absolute',
              left: 0,
              top: 0,
              width: 500,
            }}
            autoPlay
          ></video>
        </Box>
      </Box>
      <Box
        style={{
          display: 'flex',
          width: '80%',
          justifyContent: 'space-between',
        }}
      >
        <Box
          pt={2}
          style={{
            display: 'flex',
            width: 200,
            justifyContent: 'space-between',
          }}
        >
          {connectionScore <= 1 ||
            (!connectionScore && <IconAntennaBars1 size={50} color="white" />)}
          {connectionScore > 1 && connectionScore <= 2 && (
            <IconAntennaBars2 size={50} color="white" />
          )}
          {connectionScore > 2 && connectionScore <= 3 && (
            <IconAntennaBars3 size={50} color="white" />
          )}
          {connectionScore > 3 && connectionScore <= 4 && (
            <IconAntennaBars4 size={50} color="white" />
          )}
          {connectionScore > 4 && <IconAntennaBars5 size={50} color="white" />}
        </Box>
        <Box
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            width: 500,
          }}
        >
          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <ActionIcon size={50} variant="transparent">
              <IconCameraRotate size={50} color="white" />
            </ActionIcon>
            <p
              style={{
                color: 'white',
                fontSize: 12,
                textTransform: 'uppercase',
                textAlign: 'center',
              }}
            >
              Развернуть <br /> камеру
            </p>
          </Box>
          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <ActionIcon size={50} onClick={() => shareScreen()} variant="transparent">
              <IconScreenShare size={50} color="white" />
            </ActionIcon>
            <p
              style={{
                color: 'white',
                fontSize: 12,
                textTransform: 'uppercase',
                textAlign: 'center',
              }}
            >
              Демонстрация <br /> экрана
            </p>
          </Box>

          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <ActionIcon size={50} onClick={() => toggleCam()} variant="transparent">
              {isCamOn ? (
                <IconCamera size={50} color="white" />
              ) : (
                <IconCameraOff size={50} color="white" />
              )}
            </ActionIcon>
            <p
              style={{
                color: 'white',
                fontSize: 12,
                textTransform: 'uppercase',
                textAlign: 'center',
              }}
            >
              Камера
            </p>
          </Box>
          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <ActionIcon size={50} variant="transparent">
              <IconMicrophone size={50} color="white" />
            </ActionIcon>

            <p
              style={{
                color: 'white',
                fontSize: 12,
                textTransform: 'uppercase',
                textAlign: 'center',
              }}
            >
              Микрофон
            </p>
          </Box>
          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <ActionIcon size={50} variant="transparent">
              <IconFileArrowRight size={50} color="white" />
            </ActionIcon>

            <p
              style={{
                color: 'white',
                fontSize: 12,
                textTransform: 'uppercase',
              }}
            >
              Прикрепить <br /> файл
            </p>
          </Box>
        </Box>
        <Box pt={2}>
          <Stack gap={1}>
            <Button size="large" color="inherit" variant="contained">
              Профайл
            </Button>
            <Button component={Link} href={'/'} size="large" color="error" variant="contained">
              Завершить Звонок
            </Button>
          </Stack>
        </Box>
        <Stack>
          <Button onClick={() => Call()}>Позвонить</Button>
          <Text c={'#FFF'}>Номер комнаты:</Text>
          <TextInput
            placeholder="Введите номер комнаты"
            value={callId}
            onChange={(event) => setCallId(event?.currentTarget.value)}
          />
          <Button onClick={() => Answer()}>Ответить</Button>
        </Stack>
      </Box>
    </Box>
  );
}
