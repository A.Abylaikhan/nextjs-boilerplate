'use client';
import { Button, Loader, Text } from '@mantine/core';
import React, { useState } from 'react';

export default function AnswerCall() {
  const [clicked, setClicked] = useState<boolean>(false);
  const Answer = async () => {};
  return (
    <Button radius={50} size="xl" onClick={() => Answer()} variant="contained">
      {clicked ? <Loader color="#fff" /> : <Text>Ответить на звонок</Text>}
    </Button>
  );
}
