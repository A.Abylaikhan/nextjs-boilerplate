'use client';
import { BackgroundImage, Box, Button, Container, Grid, Group, Stack, Text } from '@mantine/core';
import CreateRoom from './components/CreateRoom';
import Image from 'next/image';
import {
  IconBrandFacebook,
  IconBrandInstagram,
  IconBrandTelegram,
  IconBrandTiktok,
  IconBrandWhatsapp,
  IconBrandYoutube,
  IconEdit,
  IconHeartHandshake,
  IconList,
  IconLogin2,
  IconPlaylistAdd,
  IconSquareCheck,
} from '@tabler/icons-react';
import AnswerCall from './components/AnswerCall';

export default function HomePage() {
  const instructions = [
    {
      id: 1,
      title: 'Зарегистрируйтесь на сервисе',
      icon: <IconLogin2 color="#fff" height={90} width={90} />,
    },
    {
      id: 2,
      title: 'Подайте заявку подробно описав ее',
      icon: <IconEdit color="#fff" height={90} width={90} />,
    },
    {
      id: 3,
      title: 'Выберите понравившегося специалиста',
      icon: <IconSquareCheck color="#fff" height={90} width={90} />,
    },
    {
      id: 4,
      title: 'Продолжите последующее сотрудничество через “Bats”',
      icon: <IconHeartHandshake color="#fff" height={90} width={90} />,
    },
  ];
  const services = [
    {
      id: 1,
      title: 'Популярные услуги',
      img: null,
      color: '#293C76',
    },
    {
      id: 2,
      title: 'Юриспруденция',
      img: '/uris.png',
      color: '#fff',
    },
    {
      id: 3,
      title: 'Образование',
      img: '/obr.png',
      color: '#293C76',
    },
    {
      id: 4,
      title: 'Медицина',
      img: '/med.png',
      color: '#fff',
    },
    {
      id: 5,
      title: 'IT',
      img: '/it.png',
      color: '#293C76',
    },
    {
      id: 6,
      title: 'Строительство',
      img: '/str.png',
      color: '#fff',
    },
    {
      id: 7,
      title: 'Дизайн',
      img: '/des.png',
      color: '#fff',
    },
    {
      id: 8,
      title: 'Маркетинг',
      img: '/mark.png',
      color: '#fff',
    },
    {
      id: 9,
      title: 'Съемка',
      img: '/sio.png',
      color: '#293C76',
    },
  ];
  return (
    <Container fluid p={0}>
      <Container fluid>
        <Group w={'90%'} mt={50} justify="space-between" mx={'auto'}>
          <Box w={'70%'}>
            <Text size="3vw" c={'#293C76'} fw={600} mb={50}>
              Консультация <br /> специалистов в один клик
            </Text>
            <Text size="1.5vw" c={'#293C76'} lh={'50px'}>
              Хотите получить экспертную консультацию прямо из удобного места? Встречайте
              инновационный сервис, который позволяет общаться с опытными специалистами через
              видеозвонок. Больше не нужно тратить время на поиск и запись на приемы - с
              <Text span fw={700}>
                “Bats”
              </Text>
              вы всегда будете иметь доступ к качественной консультации на расстоянии одного клика.
            </Text>
            <Button mt={50} radius={50} size="xl" color={'#293C76'}>
              <Text size="1.5vw" fw={500}>
                Подать заявку
              </Text>
            </Button>
          </Box>
          <Box w={'25%'}>
            <Box w={'100%'} style={{ position: 'relative' }} h={580}>
              <Image src={'/LPImage.png'} fill alt="LpImage" />
            </Box>
            <Group justify="center">
              <IconBrandTiktok color="#293C76" width={40} height={40} />
              <IconBrandYoutube color="#293C76" width={40} height={40} />
              <IconBrandInstagram color="#293C76" width={40} height={40} />
              <IconBrandFacebook color="#293C76" width={40} height={40} />
              <IconBrandTelegram color="#293C76" width={40} height={40} />
              <IconBrandWhatsapp color="#293C76" width={40} height={40} />
            </Group>
            <Text ta={'center'} mt={20} c={'#293C76'} fw={500} size="1vw">
              Подпиcывайтес в соцсетях
            </Text>
          </Box>
        </Group>
      </Container>
      <Container fluid mt={200} p={0}>
        <Text ml={100} size="3vw" c={'#293C76'} fw={600} mb={50}>
          Как это работает
        </Text>
        <Box
          h={550}
          w={'100%'}
          style={{
            background: 'linear-gradient(180deg, #062340 0%, rgba(0, 133, 255, 0.00) 167.38%)',
          }}
        >
          <Grid>
            {instructions.map((item) => (
              <Grid.Col key={item.id} span={3} p={0} mt={50} miw={350} mih={300}>
                <Box
                  h={'100%'}
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    borderRight: item.id !== 4 ? '3px solid #fff' : 'none',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text ta={'center'} w={200} fw={500} c={'#fff'} size="30px">
                    {item.title}
                  </Text>
                  <Box mt={'auto'}>{item.icon}</Box>
                </Box>
              </Grid.Col>
            ))}
          </Grid>
          <Box w={'100%'} my={100} style={{ display: 'flex', justifyContent: 'center' }}>
            <Button size="xl" radius={50} color="#FF0000" px={70}>
              <Text size="30px">Регистрация</Text>
            </Button>
          </Box>
        </Box>
      </Container>
      <Container fluid my={200} p={0}>
        <Box
          w={'80%'}
          mx={'auto'}
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Grid w={'100%'} mx={'auto'}>
            {services.map((item) => (
              <Grid.Col span={4} key={item.id} w={'100%'}>
                {!item.img ? (
                  <Box>
                    <Text c={'#293C76'} fw={700} size="50px" w={100}>
                      {item.title}
                    </Text>
                  </Box>
                ) : (
                  <Box w={'100%'}>
                    <BackgroundImage p={20} w={'100%'} radius={15} h={150} src={item.img}>
                      <Text c={item.color} fw={700} size={'30px'}>
                        {item.title}
                      </Text>
                    </BackgroundImage>
                  </Box>
                )}
              </Grid.Col>
            ))}
          </Grid>
          <Box
            w={'100%'}
            mt={30}
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
            }}
          >
            <Text mr={10} size="22px" c={'#293C76'} fw={600}>
              Весь список
            </Text>
            <IconList color="#293C76" size={25} />
          </Box>
        </Box>
      </Container>
      <Container
        fluid
        p={0}
        style={{
          background:
            'linear-gradient(180deg, #7DA4C9 -49.29%, #021E39 100.54%, rgba(0, 133, 255, 0.00) 184.14%)',
        }}
      >
        <Box
          w={'90%'}
          mx={'auto'}
          pt={100}
          style={{
            background:
              'linear-gradient(180deg, #7DA4C9 -49.29%, #021E39 100.54%, rgba(0, 133, 255, 0.00) 184.14%)',
          }}
        >
          <Text c={'#fff'} mb={50} size="3vw" fw={700}>
            Преимущества
          </Text>
          <Grid c={'#fff'}>
            <Grid.Col span={4}>
              <Box>
                <Box mb={50}>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Конфиденциальность
                  </Text>
                  <Text size="1.5vw">Никаких номеров телефона</Text>
                </Box>
                <Box mb={50}>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Безопасность
                  </Text>
                  <Text size="1.5vw">Полная защита данных</Text>
                </Box>
                <Box>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Профессионализм
                  </Text>
                  <Text size="1.5vw">
                    Специалисты проходят необходимые подтверждения квалификации
                  </Text>
                </Box>
              </Box>
            </Grid.Col>
            <Grid.Col span={4}>
              <Box
                pb={20}
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  gap: 40,
                  alignItems: 'center',
                }}
              >
                <Box
                  px={20}
                  py={10}
                  style={{
                    border: '3px solid #fff',
                    borderRadius: '16px',
                    width: '220px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <Image src={'/lock.svg'} alt="Lock" width={60} height={70} />
                  <Image src={'/timeFast.svg'} alt="Lock" width={60} height={70} />
                </Box>
                <Box
                  px={20}
                  py={10}
                  style={{
                    border: '3px solid #fff',
                    borderRadius: '16px',
                    width: '220px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <Image src={'/shield.svg'} alt="Lock" width={60} height={70} />
                  <Image src={'/network.svg'} alt="Lock" width={60} height={70} />
                </Box>
                <Box
                  px={20}
                  py={10}
                  style={{
                    border: '3px solid #fff',
                    borderRadius: '16px',
                    width: '220px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <Image src={'/suit.svg'} alt="Lock" width={60} height={70} />
                  <Image src={'/people.svg'} alt="Lock" width={60} height={70} />
                </Box>
              </Box>
            </Grid.Col>
            <Grid.Col span={4} style={{ textAlign: 'right' }}>
              <Box>
                <Box mb={50}>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Время приема
                  </Text>
                  <Text size="1.5vw">Моментальное соединение с понравившимся специалистом</Text>
                </Box>
                <Box mb={50}>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Соединение
                  </Text>
                  <Text size="1.5vw">Высокое качество видеозвонка</Text>
                </Box>
                <Box mb={50}>
                  <Text size="1.5vw" fw={700} lh={2}>
                    Неограниченность
                  </Text>
                  <Text size="1.5vw">Выбирайте специалистов со всего Казахстана</Text>
                </Box>
              </Box>
            </Grid.Col>
          </Grid>
        </Box>
      </Container>
    </Container>
  );
}
