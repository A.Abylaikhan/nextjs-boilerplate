'use client';
import '@mantine/core/styles.css';
import React from 'react';
import { MantineProvider, ColorSchemeScript, Box } from '@mantine/core';
import { theme } from '../theme';
import Navbar from './components/Navbar';
import { usePathname } from 'next/navigation';

export default function RootLayout({ children }: { children: any }) {
  const pathname = usePathname();
  return (
    <html lang="en">
      <head>
        <ColorSchemeScript />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
        />
      </head>
      <body>
        <MantineProvider theme={theme}>
          {!pathname.startsWith('/room') && <Navbar />}
          {children}
        </MantineProvider>
      </body>
    </html>
  );
}
