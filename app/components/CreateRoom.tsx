'use client';
import firebase from 'firebase/compat/app';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import 'firebase/compat/firestore';
import { Button, Loader, Text } from '@mantine/core';

const firebaseConfig = {
  apiKey: 'AIzaSyDXFhWl_NYVCu8C0i-EpCfU8fDCKfljyUw',
  authDomain: 'test-webrtc-56325.firebaseapp.com',
  projectId: 'test-webrtc-56325',
  storageBucket: 'test-webrtc-56325.appspot.com',
  messagingSenderId: '745315528767',
  appId: '1:745315528767:web:2fba6bb01cfc54f3b8488a',
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
const servers = {
  iceServers: [
    {
      urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
    },
  ],
  iceCandidatePoolSize: 10,
};
export default function CreateRoom() {
  const [callId, setCallId] = useState<string>('');
  const [pc, setPc] = useState<RTCPeerConnection | null>(null);
  const [clicked, setClicked] = useState<boolean>(false);
  const router = useRouter();
  const Call = async () => {
    if (!pc) return;
    setClicked(true);
    const callDoc = firebase.firestore().collection('calls').doc();
    const offerCandidates = callDoc.collection('offerCandidates');
    const answerCandidates = callDoc.collection('answerCandidates');
    setCallId(callDoc.id);
    // Create offer
    pc.onicecandidate = (event) => {
      event.candidate && offerCandidates.add(event.candidate.toJSON());
    };
    const offerDescription = await pc.createOffer();
    await pc.setLocalDescription(offerDescription);

    const offer: any = {
      sdp: offerDescription.sdp,
      type: offerDescription.type,
    };

    await callDoc.set({ offer });

    callDoc.onSnapshot((snapshot) => {
      const data = snapshot.data();
      if (!pc.currentRemoteDescription && data?.answer) {
        const answerDescription = new RTCSessionDescription(data.answer);
        pc.setRemoteDescription(answerDescription);
      }
    });
    answerCandidates.onSnapshot((snapshot: any) => {
      snapshot.docChanges().forEach((change: any) => {
        if (change.type === 'added') {
          const candidate = new RTCIceCandidate(change.doc.data());
          pc.addIceCandidate(candidate);
        }
      });
    });
    router.push(`/room/${callDoc.id}`);
  };
  useEffect(() => {
    setPc(new RTCPeerConnection(servers));
  }, []);
  return (
    <Button radius={50} size="xl" onClick={() => Call()} variant="contained">
      {clicked ? <Loader color="#fff" /> : <Text>Создать звонок</Text>}
    </Button>
  );
}
